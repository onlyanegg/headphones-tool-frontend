import React from "react"
import ModelCard from "./model-card"

class SelectedModelsContainer extends React.Component {
  constructor() {
    super()
    this.handleModelClose = this.handleModelClose.bind(this)
    this.handleModelToggleVisibility = this.handleModelToggleVisibility.bind(
      this
    )
  }

  handleModelClose(model) {
    this.props.handleModelClose(model)
  }

  handleModelToggleVisibility(model, isVisible) {
    this.props.handleModelToggleVisibility(model, isVisible)
  }

  render() {
    return (
      <div
        id="selected-models-container"
        className="col row p-0 m-0 h-100 flex-nowrap border-right"
        style={{ overflowX: "auto" }}
      >
        {this.props.selectedModels.map(model => (
          <ModelCard
            key={model.model.id}
            model={model}
            handleModelClose={this.handleModelClose}
            handleModelToggleVisibility={this.handleModelToggleVisibility}
          />
        ))}
      </div>
    )
  }
}

export default SelectedModelsContainer
