import React from "react"

class ModelDescription extends React.Component {
  constructor() {
    super()
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick() {
    this.props.handleClick(this.props.model)
  }

  render() {
    return (
      <button
        className="btn w-100 text-left border-bottom"
        onClick={this.handleClick}
      >
        <p>{this.props.model.name}</p>
      </button>
    )
  }
}

export default ModelDescription
