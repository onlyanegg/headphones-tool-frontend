import React from "react"
import { get } from "axios"
import ModalButton from "./modal-button.js"
import LogInForm from "./log-in-form.js"
import LogOutForm from "./log-out-form.js"
import SETTINGS from "../settings.js"

class UserControlsContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loggedIn: false,
    }

    this.handleLogIn = this.handleLogIn.bind(this)
    this.handleLogOut = this.handleLogOut.bind(this)
  }

  handleLogIn() {
    this.setState({ loggedIn: true })
  }

  handleLogOut() {
    this.setState({ loggedIn: false })
  }

  render() {
    const classes = "col-2 p-0"
    if (this.state.loggedIn) {
      return (
        <div className={classes}>
          <ModalButton text="Log Out">
            <LogOutForm handleLogOut={this.handleLogOut} />
          </ModalButton>
        </div>
      )
    } else {
      return (
        <div className={classes}>
          <ModalButton text="Log In">
            <LogInForm handleLogIn={this.handleLogIn} />
          </ModalButton>
        </div>
      )
    }
  }

  componentDidMount() {
    const token = localStorage.getItem("token")
    get(`${SETTINGS.baseUrl}/auth/users/me/`, {
      headers: {
        Authorization: `Token ${token}`,
      },
    }).then(resp => {
      resp.status === 200
        ? this.setState({ loggedIn: true })
        : this.setState({ loggedIn: false })
    })
  }
}

export default UserControlsContainer
