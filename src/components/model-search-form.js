import React from "react"

class ModelSearchForm extends React.Component {
  constructor() {
    super()
    this.state = {
      searchText: "",
    }

    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event) {
    const { name, value } = event.target
    this.setState({ [name]: value })
    this.props.handleChange(value)
  }

  render() {
    return (
      <div className="pt-1 pb-1">
        <input
          type="text"
          name="searchText"
          value={this.state.searchText}
          onChange={this.handleChange}
          className="form-control"
        ></input>
      </div>
    )
  }
}

export default ModelSearchForm
