import React from "react"

class TabBar extends React.Component {
  constructor() {
    super()
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick(event) {
    event.preventDefault()
    this.props.handleTabSelection(event.target.name)
  }

  render() {
    return (
      <div id="tab-bar" className="nav nav-tabs" style={{ height: "7%" }}>
        <li className="nav-item">
          <button
            name="recommendations"
            className={
              "nav-link " +
              (this.props.tabVisible === "recommendations" ? "active" : "")
            }
            onClick={this.handleClick}
            href="#"
          >
            Recommendations
          </button>
        </li>
        <li className="nav-item">
          <button
            name="distribution"
            className={
              "nav-link " +
              (this.props.tabVisible === "distribution" ? "active" : "")
            }
            onClick={this.handleClick}
            href="#"
          >
            Price Distribution
          </button>
        </li>
        <li className="nav-item">
          <button
            name="history"
            className={
              "nav-link " +
              (this.props.tabVisible === "history" ? "active" : "")
            }
            onClick={this.handleClick}
            href="#"
          >
            Price History
          </button>
        </li>
      </div>
    )
  }
}

export default TabBar
