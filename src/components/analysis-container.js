import React from "react"
import TabBar from "./tab-bar"
import TabContainer from "./tab-container"

class AnalysisContainer extends React.Component {
  constructor() {
    super()
    this.state = {
      tabVisible: "recommendations",
    }
    this.handleTabSelection = this.handleTabSelection.bind(this)
  }

  handleTabSelection(tabName) {
    this.setState({ tabVisible: tabName })
  }

  render() {
    return (
      <div
        id="analysis-container"
        className="row h-75 w-100 m-0 pt-2 border-top"
      >
        <TabBar
          tabVisible={this.state.tabVisible}
          handleTabSelection={this.handleTabSelection}
        />
        <TabContainer
          tabVisible={this.state.tabVisible}
          selectedModels={this.props.selectedModels}
        />
      </div>
    )
  }
}

export default AnalysisContainer
