import React from "react"
import "bootstrap/dist/css/bootstrap.min.css"

import ModelSelectionContainer from "./model-selection-container"
import SelectedModelsContainer from "./selected-models-container"
import UserControlsContainer from "./user-controls-container"
import AnalysisContainer from "./analysis-container"

class Layout extends React.Component {
  constructor() {
    super()
    this.state = {
      selectedModels: [],
    }

    this.handleModelSelectionModelClose = this.handleModelSelectionModelClose.bind(
      this
    )
    this.handleModelSelectionModelClick = this.handleModelSelectionModelClick.bind(
      this
    )
    this.handleModelToggleVisibility = this.handleModelToggleVisibility.bind(
      this
    )
  }

  handleModelSelectionModelClick(model) {
    this.setState(state =>
      state.selectedModels.push({ model: model, isVisible: true })
    )
  }

  handleModelSelectionModelClose(model) {
    this.setState(state => ({
      selectedModels: state.selectedModels.filter(
        value => value.model !== model
      ),
    }))
  }

  handleModelToggleVisibility(model, isVisible) {
    console.log(
      JSON.stringify(
        this.state.selectedModels.map(
          item =>
            `BEFORE - Model: ${item.model.name}, isVisible: ${item.isVisible}`
        )
      )
    )
    this.setState(
      state =>
        state.selectedModels.map(item => {
          if (item.model === model) {
            item.isVisible = isVisible
          }
          return item
        }),
      () => {
        console.log(
          JSON.stringify(
            this.state.selectedModels.map(
              item =>
                `AFTER - Model: ${item.model.name}, isVisible: ${item.isVisible}`
            )
          )
        )
      }
    )
  }

  render() {
    return (
      <div id="main-container" className="container-fluid vh-100">
        <div className="row h-100">
          <div className="col-2 h-100">
            <ModelSelectionContainer
              handleModelClick={this.handleModelSelectionModelClick}
            />
          </div>
          <div className="col-10 h-100 border-left">
            <div className="row h-25 m-0 p-0">
              <SelectedModelsContainer
                selectedModels={this.state.selectedModels}
                handleModelClose={this.handleModelSelectionModelClose}
                handleModelToggleVisibility={this.handleModelToggleVisibility}
              />
              <UserControlsContainer />
            </div>
            <AnalysisContainer selectedModels={this.state.selectedModels} />
          </div>
        </div>
      </div>
    )
  }
}

export default Layout
