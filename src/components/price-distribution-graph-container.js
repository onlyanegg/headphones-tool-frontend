import React from "react"
import Chart from "chart.js"
import { gql } from "apollo-boost"
import client from "../graphql"
import { arraysAreEqual } from "../utils"

class PriceDistributionGraphContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      graphs: [],
    }
  }

  render() {
    const visibleModels = this.props.selectedModels.filter(
      item => item.isVisible
    )

    if (visibleModels.length === 0) {
      return <p>No visible models</p>
    }

    if (visibleModels.length > 4) {
      return <p>Too many models; up to four models can be displayed.</p>
    }

    const dimensions = {
      1: ["h-100 w-100"],
      2: ["h-50 w-100", "h-50 w-100"],
      3: ["h-50 w-50", "h-50 w-50", "h-50 w-100"],
      4: ["h-50 w-50", "h-50 w-50", "h-50 w-50", "h-50 w-50"],
    }

    return (
      <div
        id="graph-container"
        className="d-flex flex-wrap h-100 w-100 border-left border-right border-bottom"
      >
        {visibleModels.map((item, index) => (
          <canvas
            key={item.model.id}
            id={"canvas-" + item.model.id}
            className={dimensions[visibleModels.length][index]}
          ></canvas>
        ))}
      </div>
    )
  }

  componentDidMount() {
    this.componentDidUpdate()
  }

  componentDidUpdate() {
    const models = this.props.selectedModels
      .filter(item => item.isVisible)
      .map(item => item.model)

    // TODO: maybe these early returns should go into modelShouldUpdate
    // Return early if the model hasn't changed
    if (arraysAreEqual(models, this.state.graphs.map(item => item.model))) {
      return
    }

    // Return early if there are no more visible models
    if (models.length === 0 || models.length > 4) {
      return
    }

    // TODO: Figure out a caching mechanism
    // Query model data and plot the result
    const query = gql`
    {
      offers(currency: "usd", available: false, models: ${JSON.stringify(
        models.map(model => model.id)
      )}) {
        curPrice
        model {
          id
        }
      }
    }
    `
    client.query({ query: query }).then(result => {
      if (result.loading) {
      }
      this.setState(
        {
          graphs: models.map(model => {
            const hist = getHistogram(
              result.data.offers
                .filter(offer => offer.model.id === model.id)
                .map(offer => offer.curPrice),
              5
            )
            let labels = []
            let data = []
            if (hist !== undefined) {
              labels = Object.keys(hist).map(price => `< \u0024${price}`)
              data = Object.values(hist)
            }
            return {
              model: model,
              chart: {
                labels: labels,
                datasets: [
                  {
                    label: `Price Distribution - ${model.name}`,
                    data: data,
                  },
                ],
              },
            }
          }),
        },
        () => {
          this.state.graphs.forEach(item => {
            const ctx = document.getElementById(`canvas-${item.model.id}`)
            new Chart(ctx, {
              type: "bar",
              data: item.chart,
              options: {
                scales: {
                  yAxes: [
                    {
                      ticks: {
                        beginAtZero: true,
                      },
                    },
                  ],
                },
              },
            })
          })
        }
      )
    })
  }
}

function getHistogram(data, numBins = 5) {
  if (data.length === 0) {
    return
  }

  data.sort((x, y) => x - y)
  const min = data[0]
  const max = data[data.length - 1]
  const diff = (max - min) / numBins

  let cur = min
  let binLimits = []
  for (var i = 0; i < numBins; i++) {
    cur = cur + diff
    binLimits.push(cur)
  }
  binLimits[binLimits.length - 1] = max

  let bins = new Array(numBins).fill(0)
  for (const [, datum] of data.entries()) {
    for (const [i, binLimit] of binLimits.entries()) {
      if (datum <= binLimit && datum > (binLimits[i - 1] || 0)) {
        bins[i] = bins[i] + 1
      }
    }
  }

  let binsObj = {}
  for (i = 0; i < numBins; i++) {
    binsObj[binLimits[i].toFixed(2)] = bins[i]
  }

  return binsObj
}

export default PriceDistributionGraphContainer
