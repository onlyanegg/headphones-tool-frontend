import React from "react"

class ModalButton extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isVisible: false,
    }

    this.handleClick = this.handleClick.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleClick(event) {
    event.preventDefault()
    switch (event.target.name) {
      case "close": {
        this.setState({ isVisible: false })
        break
      }
      case "open": {
        this.setState({ isVisible: true })
        break
      }
      default: {
        break
      }
    }
  }

  handleSubmit() {
    this.setState({ isVisible: false })
  }

  render() {
    const display = this.state.isVisible ? "d-block" : "d-none"

    return (
      <div>
        <button
          name="open"
          className="btn btn-primary"
          onClick={this.handleClick}
        >
          {this.props.text}
        </button>
        <div className={`modal ${display}`} tabIndex="-1" role="dialog">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">{this.props.text}</h5>
                <button
                  name="close"
                  className="close"
                  onClick={this.handleClick}
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  &times;
                </button>
              </div>
              <div className="modal-body">
                {React.cloneElement(this.props.children, {
                  handleSubmit: this.handleSubmit,
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default ModalButton
