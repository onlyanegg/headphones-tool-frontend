import React from "react"
import RecommendationsContainer from "./recommendations-container"
import PriceDistributionGraphContainer from "./price-distribution-graph-container"
import PriceHistoryGraphContainer from "./price-history-graph-container"

class TabContainer extends React.Component {
  render() {
    const tabMap = {
      recommendations: <RecommendationsContainer />,
      distribution: (
        <PriceDistributionGraphContainer
          selectedModels={this.props.selectedModels}
        />
      ),
      history: (
        <PriceHistoryGraphContainer
          selectedModels={this.props.selectedModels}
        />
      ),
    }

    return (
      <div id="tab-container" className="w-100" style={{ height: "93%" }}>
        {tabMap[this.props.tabVisible]}
      </div>
    )
  }
}

export default TabContainer
