import React from "react"
import { Query } from "react-apollo"
import { gql } from "apollo-boost"
import { SortedColumnHeader, SORT_DIRECTION } from "./sorted-column-header.js"

const sortColumns = {
  none: "",
  rating: "rating",
  postTimestamp: "postTimestamp",
}

class RecommendationsContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      sortBy: {
        column: "",
        direction: "",
      },
    }

    this.handleSortClick = this.handleSortClick.bind(this)
  }

  handleSortClick(column, direction) {
    this.setState({ sortBy: { column: column, direction: direction } })
  }

  // TODO: Don't make a network request if the state updates due to a sort click
  // TODO: Don't sort if the effective state hasn't changed
  render() {
    const currencyMap = {
      usd: "\u0024",
      eur: "\u20ac",
      gbp: "\u00a3",
      aud: "AU\u0024",
    }

    const query = gql`
      {
        recommendations {
          id
          offer {
            id
            curPrice
            currency
            url
            postTimestamp
            model {
              id
              name
              medianPrice
            }
          }
        }
      }
    `

    return (
      <Query query={query}>
        {({ loading, error, data }) => {
          if (loading) return <p>Loading...</p>
          if (error) return <p>Problem getting recommendations</p>

          // Add emergent properties
          data.recommendations.forEach(item => {
            item.offer.price =
              currencyMap[item.offer.currency] + item.offer.curPrice
            item.offer.rating = Math.round(
              item.offer.model.medianPrice - item.offer.curPrice
            )
            item.offer.postTimestamp = new Date(item.offer.postTimestamp)
          })

          // Sort
          data.recommendations.sort((x, y) => {
            const { ascending, descending, none } = SORT_DIRECTION
            const { column } = this.state.sortBy
            console.log(`SortBy: ${this.state.sortBy.column}`)
            switch (this.state.sortBy.direction) {
              case none: {
                return 0
              }
              case ascending: {
                return x.offer[column] - y.offer[column]
              }
              case descending: {
                return y.offer[column] - x.offer[column]
              }
              default: {
                // Shouldn't ever get here
                return 0
              }
            }
          })

          return (
            <div className="h-100 w-100 overflow-auto border-left border-right border-bottom">
              <table className="table-sm table-striped table-bordered h-100 w-100">
                <thead>
                  <tr>
                    <th>Model</th>
                    <th>Offer URL</th>
                    <th>Price</th>
                    <SortedColumnHeader
                      name={sortColumns.rating}
                      handleClick={this.handleSortClick}
                    />
                    <SortedColumnHeader
                      name={sortColumns.postTimestamp}
                      displayName="Date"
                      handleClick={this.handleSortClick}
                    />
                  </tr>
                </thead>
                <tbody>
                  {data.recommendations.map(rec => (
                    <tr key={rec.id} className="w-100">
                      <td>{rec.offer.model.name}</td>
                      <td>
                        <a
                          href={rec.offer.url}
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          {rec.offer.url}
                        </a>
                      </td>
                      <td>{rec.offer.price}</td>
                      <td>{rec.offer.rating}</td>
                      <td>{rec.offer.postTimestamp.toDateString()}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          )
        }}
      </Query>
    )
  }
}

export default RecommendationsContainer
