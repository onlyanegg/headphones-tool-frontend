import React from "react"
import Chart from "chart.js"
import { gql } from "apollo-boost"
import client from "../graphql"
import { arraysAreEqual } from "../utils"

class PriceHistoryGraphContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      models: [],
      chart: {},
    }
  }

  render() {
    return (
      <div
        id="graph-container"
        className="d-flex h-100 w-100 border-left border-right border-bottom"
      >
        <canvas id="price-history-canvas" className="h-100 w-100"></canvas>
      </div>
    )
  }

  componentDidMount() {
    this.componentDidUpdate()
  }

  componentDidUpdate() {
    const models = this.props.selectedModels
      .filter(item => item.isVisible)
      .map(item => item.model)

    // TODO: maybe these early returns should go into modelShouldUpdate
    // Return early if the model hasn't changed
    if (arraysAreEqual(models, this.state.models)) {
      return
    }

    // Return early if there are no more visible models
    if (models.length === 0) {
      return
    }

    const colors = ["#0F7173", "#F7567C", "#C5EBC3", "#99E1D9", "#5D576B"]

    // TODO: Figure out a caching mechanism
    // Query model data and plot the result
    const query = gql`
      {
        offers(available: false, currency: "usd", models: ${JSON.stringify(
          models.map(model => model.id)
        )}) {
          curPrice
          saleTimestamp
          model {
            id
          }
        }
      }
    `
    client.query({ query: query }).then(result => {
      if (result.loading) {
      }
      this.setState(
        {
          models: models,
          chart: {
            datasets: models.map((model, index) => ({
              label: model.name,
              pointStyle: "circle",
              backgroundColor: colors[index], // Point color
              borderColor: colors[index], // Line color
              fill: false, // Don't fill under line
              lineTension: 0, // Draw straight lines
              data: result.data.offers
                .filter(item => item.model.id === model.id)
                .map(item => ({
                  t: new Date(item.saleTimestamp),
                  y: item.curPrice,
                })),
            })),
          },
        },
        () => {
          const ctx = document.getElementById("price-history-canvas")
          new Chart(ctx, {
            type: "line",
            data: this.state.chart,
            options: {
              scales: {
                xAxes: [
                  {
                    type: "time",
                  },
                ],
              },
            },
          })
        }
      )
    })
  }
}

export default PriceHistoryGraphContainer
