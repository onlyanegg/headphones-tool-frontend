import React from "react"

const SORT_DIRECTION = {
  none: null,
  ascending: "ascending",
  descending: "descending",
}

class SortedColumnHeader extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      sort: null,
    }

    this.handleClick = this.handleClick.bind(this)
  }

  handleClick(event) {
    event.preventDefault()
    event.persist() // Needed to access event.target within setState

    this.setState(
      state => {
        const { ascending, descending, none } = SORT_DIRECTION
        switch (state.sort) {
          case none: {
            return {
              sort: event.target.name,
            }
          }
          case ascending: {
            if (event.target.name === ascending) {
              return {
                sort: none,
              }
            } else {
              return {
                sort: descending,
              }
            }
          }
          case descending: {
            if (event.target.name === descending) {
              return {
                sort: none,
              }
            } else {
              return {
                sort: ascending,
              }
            }
          }
          default: {
            // Shouldn't ever get here
            console.log(`Invalid sort selection: ${state.sort}`)
            break
          }
        }
      },
      () => {
        this.props.handleClick(this.props.name, this.state.sort)
      }
    )
  }

  render() {
    const { ascending, descending } = SORT_DIRECTION
    return (
      <th>
        <div name={this.props.name} className="d-flex justify-content-between">
          {this.props.displayName !== undefined
            ? this.props.displayName
            : capitalize(this.props.name)}
          <div style={{ width: "1em" }}>
            <button
              name={ascending}
              className="d-block w-100"
              style={{ height: "50%" }}
              onClick={this.handleClick}
            >
              &#x2227;
            </button>
            <button
              name={descending}
              className="d-block w-100"
              style={{ height: "50%" }}
              onClick={this.handleClick}
            >
              &#x2228;
            </button>
          </div>
        </div>
      </th>
    )
  }
}

function capitalize(word) {
  return word.charAt(0).toUpperCase() + word.slice(1)
}

export { SortedColumnHeader, SORT_DIRECTION }
