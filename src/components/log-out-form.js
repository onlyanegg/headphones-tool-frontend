import React from "react"
import { post } from "axios"
import SETTINGS from "../settings.js"

class LogOutForm extends React.Component {
  constructor(props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit(event) {
    event.preventDefault()
    const token = localStorage.getItem("token")
    post(
      `${SETTINGS.baseUrl}/auth/token/logout/`,
      {},
      {
        headers: { Authorization: `Token ${token}` },
      }
    ).then(resp => {
      if (resp.status === 204) {
        this.props.handleSubmit()
        this.props.handleLogOut()
        localStorage.removeItem("token")
      }
    })
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <button
          name="submit"
          type="submit"
          className="form-control btn btn-primary"
        >
          Log out
        </button>
      </form>
    )
  }
}

export default LogOutForm
