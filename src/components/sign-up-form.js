import React from "react"
import { post } from "axios"
import SETTINGS from "../settings.js"

class LogInForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      email: "",
      password: "",
      passwordConfirm: "",
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange(event) {
    event.preventDefault()
    this.setState({
      [event.target.id]: event.target.value,
    })
  }

  handleSubmit(event) {
    event.preventDefault()
    this.props.handleSubmit()
    //post(`${SETTINGS.baseUrl}/auth/token/login/`, this.state).then(resp => {
    //  if (resp.status == 200) {
    //    this.props.handleLogIn()
    //    localStorage.setItem("token", resp.data.auth_token)
    //  }
    //})
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label htmlFor="email">Email</label>
          <input id="email" type="text" className="form-control" />
        </div>
        <div className="form-group">
          <label htmlFor="password">Password</label>
          <input id="password" type="password" className="form-control" />
        </div>
        <div className="form-group">
          <input
            id="passwordConfirm"
            type="password"
            className="form-control"
          />
        </div>
        <button type="submit" className="form-control btn btn-primary">
          Sign Up
        </button>
      </form>
    )
  }
}

export default LogInForm
