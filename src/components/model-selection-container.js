import React from "react"
import { gql } from "apollo-boost"
import ModelSearchForm from "./model-search-form.js"
import ModelDescription from "./model-description.js"
import client from "../graphql.js"

class ModelSelectionContainer extends React.Component {
  constructor() {
    super()
    this.state = {
      page: 0,
      searchText: "",
      models: [],
    }

    this.handleClick = this.handleClick.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)

    fetchModels(this.state.page, this.state.searchText).then(result => {
      if (result.loading) {
      }
      this.setState({
        models: result.data.models,
      })
    })
  }

  handleInputChange(searchText) {
    fetchModels(0, searchText).then(result => {
      if (result.loading) {
      }
      this.setState({
        page: 0,
        searchText: searchText,
        models: result.data.models,
      })
    })
  }

  handleClick(event) {
    event.preventDefault()
    var newPage = this.state.page + 1
    fetchModels(newPage, this.state.searchText).then(result => {
      if (result.loading) {
      }
      this.setState(state => {
        var newModels = [...state.models]
        newModels.push(...result.data.models)
        return {
          page: newPage,
          models: newModels,
        }
      })
    })
  }

  render() {
    return (
      <div id="model-selection-container" className="h-100">
        <ModelSearchForm handleChange={this.handleInputChange} />
        <div id="model-list" style={{ height: "93%" }}>
          <div className="overflow-auto h-100">
            {this.state.models.map(model => (
              <ModelDescription
                key={model.id}
                model={model}
                handleClick={this.props.handleModelClick}
              />
            ))}
            <button
              className="btn btn-secondary w-100"
              onClick={this.handleClick}
            >
              more
            </button>
          </div>
        </div>
      </div>
    )
  }
}

function fetchModels(page, searchText) {
  const pageSize = 20
  const query = gql`
      {
        models(first: ${pageSize}, skip: ${page *
    pageSize}, orderBy: ["-num_sold"], contains: "${searchText}") {
          id
          name
        }
      }
    `

  return client.query({ query: query })
}

export default ModelSelectionContainer
