import React from "react"
import { gql } from "apollo-boost"
import { Query } from "react-apollo"
import { currencies } from "../utils.js"

class ModelCard extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      model: props.model.model,
      isVisible: props.model.isVisible,
    }
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick(event) {
    event.preventDefault()

    switch (event.target.name) {
      case "close": {
        this.props.handleModelClose(this.state.model)
        break
      }
      case "toggleGraphVisibility": {
        // State is not guaranteed to change right after setState, so we define the value to be
        // propogated here
        const isVisible = !this.state.isVisible
        this.setState({ isVisible: isVisible })
        this.props.handleModelToggleVisibility(this.state.model, isVisible)
        break
      }
      default:
        break
    }
  }

  render() {
    // TODO: Should stats be collected for all the models in the ModelList instead of making
    // individual requests here? In otherwords, should we make one request with a lot of data in
    // the beginning or several small requests during the session?
    //
    // My intuition says one request in the beginning is better for user experience. I could even
    // batch the requests, so that the page seems more reponsive.
    console.log(`Model ID: ${this.state.model.id}`)
    const query = gql`
      {
        model(id: ${this.state.model.id}){
          id
          avgPrice
          medianPrice
        }
      }
    `
    return (
      <Query query={query}>
        {({ loading, error, data }) => {
          if (loading) {
            return <div className="col-3"></div>
          }

          console.log(`Data: ${JSON.stringify(data)}`)
          return (
            <div className="col-3">
              <div className="card">
                <div className="card-header btn-group p-0">
                  <button
                    name="toggleGraphVisibility"
                    type="button"
                    value={this.state.isVisible ? "&#x2714;" : ""}
                    className="btn btn-sm btn-secondary rounded-0"
                    onClick={this.handleClick}
                  >
                    {this.state.isVisible ? "hide" : "show"}
                  </button>
                  <button
                    name="close"
                    type="button"
                    className="btn btn-sm btn-secondary rounded-0"
                    onClick={this.handleClick}
                  >
                    close
                  </button>
                </div>
                <div className="card-body pt-1 pb-1">
                  <h5 className="card-title h-50 overflow-hidden">
                    {this.state.model.name}
                  </h5>
                  <table className="table-sm table-bordered">
                    <tbody>
                      <tr>
                        <td className="col">avg</td>
                        <td className="col">
                          {error
                            ? ""
                            : currencies.js.usd +
                              Math.floor(data.model.avgPrice)}
                        </td>
                      </tr>
                      <tr>
                        <td className="col">median</td>
                        <td className="col">
                          {error
                            ? ""
                            : currencies.js.usd +
                              Math.floor(data.model.medianPrice)}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          )
        }}
      </Query>
    )
  }
}

export default ModelCard
