import React from "react"
import { post } from "axios"
import SETTINGS from "../settings.js"

class LogInForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      username: "",
      password: "",
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange(event) {
    event.preventDefault()
    this.setState({
      [event.target.id]: event.target.value,
    })
  }

  handleSubmit(event) {
    event.preventDefault()
    post(`${SETTINGS.baseUrl}/auth/token/login/`, this.state).then(resp => {
      if (resp.status === 200) {
        localStorage.setItem("token", resp.data.auth_token)
        this.props.handleSubmit()
        this.props.handleLogIn()
      }
    })
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label htmlForm="username">Username</label>
          <input
            id="username"
            type="text"
            className="form-control"
            value={this.state.username}
            onChange={this.handleChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="password">Password</label>
          <input
            id="password"
            type="password"
            className="form-control"
            value={this.state.password}
            onChange={this.handleChange}
          />
        </div>
        <button type="submit" className="form-control btn btn-primary">
          Log in
        </button>
      </form>
    )
  }
}

export default LogInForm
