import React from "react"
import { ApolloProvider } from "react-apollo"

import "../css/base.css"
import client from "../graphql.js"
import Layout from "../components/layout.js"

class IndexPage extends React.Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <div>
          <Layout />
        </div>
      </ApolloProvider>
    )
  }
}

export default IndexPage
