function arraysAreEqual(arr1, arr2) {
  if (arr1.length !== arr2.length) return false
  if (!arr1.every((item, index) => item === arr2[index])) return false
  return true
}

const currencies = {
  js: {
    usd: "\u0024",
  },
}

export { arraysAreEqual, currencies }
