//import dotenv from "dotenv"
//dotenv.config()

const SETTINGS = {
  //baseUrl: "http://headphones-backend.onlyanegg.me",
  //baseUrl: "https://headphones-tool.herokuapp.com",
  //baseUrl: "http://localhost:7000",
  baseUrl: process.env.BASE_URL,
}

export default SETTINGS
