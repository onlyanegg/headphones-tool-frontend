import ApolloClient from "apollo-boost"
import fetch from "node-fetch"
import SETTINGS from "./settings"

const client = new ApolloClient({
  uri: `${SETTINGS.baseUrl}/graphql/`,
  fetch: fetch,
})

export default client
